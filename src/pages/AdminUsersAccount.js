import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

const UserList = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {

    const fetchUsers = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/allUser`); 
        const data = await response.json();
        setUsers(data);
        console.log(data)

      } 
      catch (error) 
      {
        console.error('Failed to fetch users:', error);
      }
    };

    fetchUsers();
  }, []);

  return (
    <div id='containerBox'>
     <div className="table-responsive" style={{width: '100%'}}>
      <h1><strong>USERS LIST</strong></h1>
      <table style={{ width: "100%" }}>
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Mobile Number</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user => (
            <tr key={user.id}>
              <td>{user._id}</td>
              <td>{user.first_Name}</td>
              <td>{user.last_Name}</td>
              <td>{user.email}</td>   
              <td>{user.mobileNo}</td>
              <td>
                <Button className="bg-primary" as={Link} to={`/editProfile/${user._id}`}>Edit Profile</Button>
              </td> 
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  );
};

  
        
export default UserList;
