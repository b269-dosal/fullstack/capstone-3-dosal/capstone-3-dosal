import React, { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

export default function Cart() {
    const { user } = useContext(UserContext);
    const USERID = user.id;
    console.log(USERID)
    const userADMIN = user.isAdmin;
    console.log(userADMIN)
    const [orders, setOrders] = useState([]);
    const [users, setUsers] = useState([]);

    useEffect(() => {
        if (userADMIN) 
        {
            const fetchOrdersMadeAdmin = async () => {
                try {
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/order/transaction2`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                            //'Authorization': `Bearer ${localStorage.getItem('token')}` 
                        }
                    });

                    if (response) {
                        const data = await response.json();
                        
                        const userIds = data.map(transaction => transaction.userId);
                        
                        data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
                        console.log("Array of userIds: ", userIds);
                        console.log("ARRAYS of Orders: ")
                        console.log(data)
                        setOrders(data); 
                        

                    } else {
                        console.error(`Failed to fetch orders made: ${response.status} ${response.statusText}`);
                    }
                } 
                    catch (error) {
                    console.error(`Failed to fetch orders made: ${error}`);
                }

            }

        fetchOrdersMadeAdmin();

        }
                else    {
                            console.log("NON ADMIN NOT ALLOWED")
                            window.location.href = `/`;
                        }

                   
            
            console.log("Name of the Buyer: ")
           // console.log(userName)

            const fetchNameofBuyer = async () => {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/users/${USERID}/userDetails`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}` 
                    }

                });
                if (response) {
                    const data = await response.json();
                    setUsers(data); 
                    console.log("Details of Buyer: ")
                    console.log(data)
                    console.log(users)
                }
            }
            fetchNameofBuyer();

    }, []);

    return (
        <div id="containerOrdersMade" >
          <div className="table-responsive" style={{width: '100%'}}>
            <h1><strong>ORDERS MADE BY COSTUMER</strong></h1>
            <table>
                <thead>
                    <tr>
                        <th>Transaction ID</th> 
                        <th>Customer Email</th>        
                        {/* <th>Transaction Date</th> */}
                        <th>Status</th>
                      {/*   <th>Product Ids</th> */}
                        <th>Product Name</th>
                        <th>Quantity</th>  
                      {/*   <th>Product Prices</th>   */}
                        <th>Transaction Total Amount</th>                  
                    </tr>
                </thead>
                <tbody>
                    {orders.map(order => (
                        <tr key={order.id}>

                            <td>{order._id}</td>
                            <td>{order.email}</td>
                           {/*   <td>{order.createdAt}</td>  */}
                            <td>{order.status}</td>
                            {/* <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.productId}</div>
                                ))}
                            </td> */}
                            <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.productName}</div>
                                ))}
                            </td>
                            <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.quantity}</div>
                                ))}
                            </td>
                          {/*   <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.price}</div>
                                ))}
                            </td> */}
                            <td>{order.amount}</td>

                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        </div>
    );
}




//////////


