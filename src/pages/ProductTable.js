import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";

const ProductTable = ({ products, categories }) => {
    const [sortColumn, setSortColumn] = useState("");
    const [sortOrder, setSortOrder] = useState("");
    const [currentPage, setCurrentPage] = useState(1);
    const [productsPerPage] = useState(5);

    useEffect(() => {
        console.log("Products changed:", products);
    }, [products]);

    const handleSort = (column) => {
        if (sortColumn === column) {
            setSortOrder(sortOrder === "asc" ? "desc" : "asc");
        } else {
            setSortColumn(column);
            setSortOrder("asc");
        }
    };

    const handleDeactivateProduct = (productId) => {
        console.log(`Disable product with ID: ${productId}`);
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({ isActive: false }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(`Product with ID ${productId} has been disabled.`);
            })
            .catch((error) => {
                console.error(`Error disabling product with ID ${productId}:`, error);
            });
        window.location.href = `/allProducts`;
    };

    const handleReactivateProduct = (productId) => {
        console.log(`Enable product with ID: ${productId}`);
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({ isActive: true }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(`Product with ID ${productId} has been enable.`);
            })
            .catch((error) => {
                console.error(`Error disabling product with ID ${productId}:`, error);
            });
        window.location.href = `/allProducts`;
    };

    const sortProducts = (products) => {
        if (sortColumn === "") {
            return products;
        }

        const sortedProducts = [...products].sort((a, b) => {
            if (a[sortColumn] < b[sortColumn]) {
                return sortOrder === "asc" ? -1 : 1;
            } else if (a[sortColumn] > b[sortColumn]) {
                return sortOrder === "asc" ? 1 : -1;
            } else {
                return 0;
            }
        });

        return sortedProducts;
    };

    const sortedProducts = sortProducts(products);

    const readableValue = (value) => {
        return value ? "Yes" : "No";
    };

    const handleUpdateProduct = (productId) => {
        console.log(typeof productId);
        window.location.href = `update/${productId}`;

        return console.log(productId);
    };

    // Logic for displaying products with pagination
    const indexOfLastProduct = currentPage * productsPerPage;
    const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
    const currentProducts = sortedProducts.slice(indexOfFirstProduct, indexOfLastProduct);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);


    return (
        <div id="containerBoxAllProduct">
            <h1>
                <strong>ALL PRODUCT</strong>
            </h1>
            <div className="table-responsive" style={{width: '100%'}}>
            <table id="productTable">

            <thead>
                <tr>
                    <th onClick={() => handleSort("productName")}>Product Name</th>
                    <th onClick={() => handleSort("category")}>Category</th>
                    <th onClick={() => handleSort("price")}>Price</th>
                    <th onClick={() => handleSort("isActive")}>Stocks</th>
                    <th onClick={() => handleSort("isActive")}>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {currentProducts.map((product) => (
                    <tr key={product._id}>
                        <td>{product.prod_name}</td>
                        <td>{product.category}</td>
                        <td>{product.prod_price}</td>
                        <td>{product.stocks}</td>
                        <td>{readableValue(product.isActive)}</td>
                        <td>
                            <Button onClick={() => handleUpdateProduct(product._id)}>
                                Update
                            </Button>
                            <Button
                                onClick={() =>
                                    product.isActive && product.stocks !== 0
                                        ? handleDeactivateProduct(product._id)
                                        : product.stocks === 0
                                        ? null 
                                        : handleReactivateProduct(product._id)
                                }
                                variant="success"
                                disabled={product.stocks === 0} 
                            >
                                {product.isActive ? "Deactivate" : "Reactivate"}
                            </Button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
        <div id="pagination">
            {Array.from({ length: Math.ceil(sortedProducts.length / productsPerPage) }).map(
                (_, index) => (
                    <button key={index} onClick={() => paginate(index + 1)}>
                        {index + 1}
                    </button>
                )
            )}
        </div>
        </div>    
    </div>
);
   
}

export default ProductTable;










