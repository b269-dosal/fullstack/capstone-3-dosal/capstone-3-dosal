import React, { useEffect, useState, useContext } from "react";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";
import { Button } from "react-bootstrap";

export default function Cart() {
  

  const { user } = useContext(UserContext);
  const USERID = user.id; 
  console.log(USERID)

  const [cartItems, setCartItems] = useState([]);
  const [totalAMOUNT, settotalAMOUNT] = useState([]);
 



useEffect(() => {
  const fetchCartItems = () => {
    if (!user.isAdmin) {
      fetch(`${process.env.REACT_APP_API_URL}/order/cartItems2/${USERID}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}` 
        } 
      })
      .then(res => res.json())
      .then(data => {
      console.log(data)
        const filteredItems = data.filter(item => item.status === "Add to Cart");
        console.log('Filtered Items:', filteredItems);

        const updatedItems = filteredItems.map(item => {
          const updatedTotalAmount = item.productPrice * item.quantity;
          return { ...item, totalAmount: updatedTotalAmount };
        });
        console.log(updatedItems)
        setCartItems(updatedItems);


        const totalSum = updatedItems.reduce((sum, item) => sum + item.totalAmount, 0);
        console.log('Total Sum:', totalSum);
        settotalAMOUNT(totalSum)

    
      })
      .catch(error => {

      });
   }
     else {
      return console.log("Not Allow")
    } 
  };

  
  fetchCartItems();
}, [USERID, user.isAdmin]);



////////// Quantity Change

const handleQuantityChange = (orderId, e) => {
  const newQuantity = e.target.value;
  setCartItems(prevCartItems => {
    const updatedCartItems = prevCartItems.map(item => {
      if (item._id === orderId) {
        const updatedTotalAmount = item.productPrice * newQuantity; // Calculate updated total amount
        console.log(updatedTotalAmount)
        return { ...item, quantity: newQuantity, totalAmount: updatedTotalAmount }; 
      }
      return item;
    });

    // Calculate updated total amount based on updated cart items
    const updatedTotalAmount = updatedCartItems.reduce((total, item) => {
      return total + item.totalAmount;
    }, 0);

    settotalAMOUNT(updatedTotalAmount); // Set the updated total amount

    console.log(`SubTotal of all "Add to Cart" Status: ${updatedTotalAmount}`)
    console.log(updatedCartItems)
    return updatedCartItems;
  });
};



//// [PRODUCT REMOVAL]
const handleRemoveFromCart = (ordertoDelete) => {
  console.log(`Ordered ID to Delete: ${ordertoDelete}`)
  fetch(`${process.env.REACT_APP_API_URL}/order/deleteOrderCart/${USERID}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}` 
    },
    body: JSON.stringify({
      orderId: ordertoDelete
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)

    setCartItems(prevCartItems => prevCartItems.filter(item => item._id !== ordertoDelete));

    // Calculate updated total amount based on updated cart items
    const updatedTotalAmount = cartItems.reduce((total, item) => {
      return total + item.totalAmount;
    }, 0);

    settotalAMOUNT(updatedTotalAmount); // Set the updated total amount

    Swal.fire({
      title: "Product Removed",
      icon: "success",
      text: "You have successfully removed the product from the cart."
    }).then(() => {
      window.location.href = '/cart'
    })

  })
  .catch(error => {
    console.log(error)
    Swal.fire({
      title: "Error",
      icon: "error",
      text: "Failed to remove the product from the cart. Please try again."
    });
  });
};



////////////// [END OF PRODUCT REMOVAL]



 const handleUpdateOrder = (OrderId, updatingQuantity, updatingTotal) => {
  console.log(`   `)


  const updatedItem = cartItems.find(item => item._id === OrderId);
  console.log(updatedItem)

  const PRODUCTNAME = updatedItem.productName;


    fetch(`${process.env.REACT_APP_API_URL}/order/updateQuantity/${USERID}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}` 
    },
    body: JSON.stringify({
      userId: USERID,
      orderId: OrderId,
      quantity: updatingQuantity,
      totalAmount: updatingTotal
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data);

    if (!user.isAdmin) {
      Swal.fire({
          title: "Updating Product",
          icon: "success",
          text: `You have successfully Updated ${PRODUCTNAME}`
      }).then(() => {                                                              
      });
  } 
  else                               
  {
      Swal.fire({
          title: "Error",
          icon: "error",
          text: "Please try again."
      });
  }

  })
  .catch(error => {
    console.log(`What Happen?${error}`)

  });   

};


/////////// checkout Order and update Stock and Update Status of Order

const checkoutOrders = (CheckoutItems) => {
  console.log("")
  console.log(CheckoutItems)
  console.log(`UserID to Checkout: `);
  console.log(USERID);
  console.log("Total Amount of all Orders: ");
  console.log(totalAMOUNT);

   const updatedCartItems = cartItems.map(order => {
      order.status = "COMPLETED";
   
    return order;
  });
  console.log(`Updated Status: `)
  console.log(updatedCartItems)
   
    fetch(`${process.env.REACT_APP_API_URL}/order/checkout2/${USERID}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}` 
    },
    body: JSON.stringify({
      userId: USERID
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)

    setCartItems(checkoutOrders => checkoutOrders.filter(item => item.status === "COMPLETED"));
   
    Swal.fire({
      title: "Checkout Success",
      icon: "success",
      text: `You have successfully Checkout the Orders, Amount: ${totalAMOUNT}`
    }).then(() => {
      window.location.reload();
    });
  })
  
  .catch(error => {
    console.log(error)
    Swal.fire({
      title: "Error",
      icon: "error",
      text: "Failed to remove the product from the cart. Please try again."
    });
  });  

};

return (
  <div id="containerBox" style={{ borderRadius: '30px' }} >
      <div className="text-center">
      <h1 id="header_font">CART ITEMS</h1>
    </div>
    <div className="container">
      <div className="row">
        {cartItems.map(order => (
          <div className="col-md-6" key={order._id} >
            <div className="card" id="cartItems">
              <div className="card-body">
                <h3 className="card-title" id="#cart-item-names">{order.productName}</h3>
                <p className="card-text">Price: {order.productPrice}</p>
                <p className="card-text">Quantity: 
                  <input
                    type="number"
                    value={order.quantity}
                    onChange={(e) => {
                      const newQuantity = parseInt(e.target.value);
                      if (newQuantity > 0) { 
                        handleQuantityChange(order._id, e);
                      } else {
                        e.target.value = 1;
                      }
                    }}
                  />
                </p>
                <p className="card-text">Total Amount: {order.totalAmount}</p>
                <div className="row">
                  
                    <Button variant="danger" onClick={() => handleRemoveFromCart(order._id)}>Remove</Button>
                
                 
                    <Button variant="primary" onClick={() => handleUpdateOrder(order._id, order.quantity, order.totalAmount)}>Update</Button>
                
                </div>
              </div>
            </div>
            <br />
          </div>
        ))}
      </div>
      <div className="row">
        <div className="col-md-12">
                  
        <div id="totalAmount" class="form-control">{totalAMOUNT}</div>
     
        <Button variant="success" style={{ width: '100%' }} onClick={() => checkoutOrders(cartItems)}><h2>CHECKOUT</h2></Button>




        </div>
      </div>
    </div>
  </div>
);





};