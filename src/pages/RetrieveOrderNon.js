import React, { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

export default function Cart() {
    const { user } = useContext(UserContext);
    const USERID = user.id;
    const userADMIN = user.isAdmin;
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        const fetchOrdersMade = async () => {
            if (!userADMIN) {
                try {
                    const requestBody = { userId: USERID };
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/order/transactionUser`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify(requestBody)
                    });
                    if (response) {
                        const data = await response.json();
                        
                        const sortedData = data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
                        setOrders(sortedData); 
                    } else {
                        console.error(`Failed to fetch orders made: ${response.status} ${response.statusText}`);
                    }
                } catch (error) {
                    console.error(`Failed to fetch orders made: ${error}`);
                }
            } else {
                console.log("NOT BELONG HERE");
                window.location.href = `/`;
            }
        };

        fetchOrdersMade();
    }, []);

    return (
        <div id="containerBox" style={{ borderRadius: '30px' }}>
       
            <div className="text-center">
            <h1 id="header_font">Orders</h1></div>
            <div className="table-responsive" style={{width: '100%'}}>
            <table className="table">
           
                <thead>
                    <tr>
                        <th>Transaction ID</th>
                        <th>Purchased Date</th>
                        <th>Status</th>
                        <th>Products</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {orders.map(order => (
                        <tr key={order.id}>
                            <td>{order._id}</td>
                            <td>{order.createdAt}</td>
                            <td>{order.status}</td>
                            <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.productName}</div>
                                ))}
                            </td>
                            <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.quantity}</div>
                                ))}
                            </td>
                            <td>
                                {order.productsPurchased.map(product => (
                                    <div key={product.productId}>{product.price}</div>
                                ))}
                            </td>
                            <td>{order.amount}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            </div>
        </div>
    );
    
}
