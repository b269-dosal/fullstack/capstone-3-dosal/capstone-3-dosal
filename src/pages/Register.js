import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import "./Login.css";


export default function Register() {

    const navigate = useNavigate();
    const {user} = useContext(UserContext);
    const [firstName, setfname] = useState("");
    const [lastName, setlname] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setnumber] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect (() => {
        if ((email !== "" && password1 !== "" && password2 !== ""  && firstName !== "" && lastName !== "" && lastName !== "" && mobileNo !== "")  && password1 === password2)
            {
                setIsActive(true);           
            }
            else {
                setIsActive(false)
            }
    }, [email, password1, password2, firstName, lastName, mobileNo]);   

    

    function registerUser(e) {
        e.preventDefault();
   
        setfname("");
        setlname("");
        setEmail("");
        setnumber("");
        setPassword1("");
        setPassword2("");
       
    
        const register = () => {
            fetch(`${process.env.REACT_APP_API_URL}/users/emailcheck`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email 
                })
            })
            .then(res => res.json())
            .then(data => {

                if (data) {
                    Swal.fire({
                        title: "Email already exists",
                        icon: "error",
                        text: "Please choose a different email."
                    });
                    console.log(data)
                }
            
                     else {
                        fetch(`${process.env.REACT_APP_API_URL}/users/userRegistration`, {
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                first_Name: firstName,
                                last_Name: lastName,
                                email: email,
                                password: password1,
                                mobileNo: mobileNo                                
                            })
                        })
                            .then(res => res.json())
                            .then(data => {
                                console.log(data);
    
                                if (data) {
                                    Swal.fire({
                                        title: "Registration Succesful",
                                        icon: "success",
                                        text: "You have successfully registered. Please login to continue."
                                    }).then(() => {
                                      
                                        navigate('/login');
                                    });
                                } else {
                                    Swal.fire({
                                        title: "Something went wrong",
                                        icon: "error",
                                        text: "Please try again."
                                    });
                                }
                            });
                    }
                    
                });
                
        };
    
        register();
    }

    const handleReturnClick = () => {        
        window.location.href = `/login`;
        };
     

    return (
       (user.id !== null) ? 
       <Navigate to="/courses"/>  
       :  
       <div id='editProfile'>
       <div className="login-container2">
        <Form onSubmit={(e) => registerUser(e)} id="form">


            <Form.Group controlId="firstName" id='form2'>
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="string" 
	                placeholder="Enter First Name" 
                    value={firstName}
                    onChange={e => setfname(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                   
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="string" 
	                placeholder="Enter Last Name" 
                    value={lastName}
                    onChange={e => setlname(e.target.value)}
	                required
                />
            
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>


            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Mobile Number" 
                    value={mobileNo}
                    onChange={e => setnumber(e.target.value)}
	                required
                />
            </Form.Group>


            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
	                required
                />
                
            </Form.Group>
           <p></p>
            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
            	Submit    </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
            	Submit    </Button>
            }
            
            <Button type="submit" variant="secondary" onClick={handleReturnClick}  style={{ width: '100%' }}>
          Return
            </Button>

        </Form> 
        </div>
        </div>
    )
}



