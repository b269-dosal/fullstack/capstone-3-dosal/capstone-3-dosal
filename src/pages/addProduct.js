import React, { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
//import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from "react-router-dom";
import './Login.css'


export default function RegisterProduct() {

  //const navigate = useNavigate();
  const {user} = useContext(UserContext);
  
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [productPrice, setPrice] = useState("");
  const [stocks, setStocks] = useState("");
  const [category, setCategory] = useState("");
 // const [categories, setCategories] = useState([])

  const [isActive, setIsActive] = useState("");

  useEffect (() => {
      if (productName !== "" && description !== "" && productPrice !== "" && stocks !== "" && category !== "")
          {
              setIsActive(true);           
          }
          else {
              setIsActive(false)
          }
  }, [productName, description, productPrice, stocks, category]);     

  /// [PRODUCT REGISTRATION]
  function registerProduct(e) {
      e.preventDefault();
 
        setProductName("");
      setDescription("");
      setDescription("");
      setPrice("");
      setStocks(""); 
      setCategory("");        
  
      const addingProduct = () => {

                      fetch(`${process.env.REACT_APP_API_URL}/products/addproduct`, {
                          method: "POST",
                          headers: {
                              'Content-Type': 'application/json',
                               Authorization: `Bearer ${localStorage.getItem('token')}`
                          },

                          body: JSON.stringify({
                            prod_name: productName,
                            prod_desc: description,
                            prod_price: productPrice,
                            stocks: stocks,   
                            category: category                                                
                          })
                          
                      })
                          .then(res => res.json())
                          .then(data => {
                              console.log(data);
                          
  
                              if (user.isAdmin) {
                                  Swal.fire({
                                      title: "Adding Product Succesful",
                                      icon: "success",
                                      text: "You have successfully registered New Product"
                                  }).then(() => {                                                              
                                  });
                              } else                               
                              {
                                  Swal.fire({
                                      title: "Not Admin function",
                                      icon: "error",
                                      text: "Please try again."
                                  });
                              }
                          });
                }  
      addingProduct();
  }
  //// [END OF PRODUCT REGISTRATION]


  return (
    (user.isAdmin !== true) ? 
       <Navigate to="/"/> 
       :

<div className="login-container2" id="containerAddProduct">
   <Form onSubmit={(e) => registerProduct(e)} id="form">


   <Form.Group controlId="ProductName">
       <Form.Label>Product Name</Form.Label>
       <Form.Control 
         type="string" 
         placeholder="Enter Name of the Product" 
           value={productName}
           onChange={e => setProductName(e.target.value)}
         required
       />
       <Form.Text className="text-muted">  </Form.Text>
   </Form.Group> 


   <p></p>
   <Form.Group controlId="Description">
   <Form.Label>Description</Form.Label>
   <textarea
          id="form2"
          className="form-control"
          value={description}
          onChange={e => setDescription(e.target.value)}
          rows={4}
          cols={40}
          placeholder="Description Here"
        />
    </Form.Group>


<p></p>


   <Form.Group controlId="ProductPrice">
      <Form.Label>Product Price</Form.Label>
       <Form.Control 
         type="number" 
         placeholder="Set Product Price" 
           value={productPrice}
           onChange={e => setPrice(e.target.value)}
         required
       />
       <Form.Text className="text-muted">  </Form.Text>
   </Form.Group> 
   <p></p>


   <Form.Group controlId="ProductStocks">
      <Form.Label>Input Number of Stocks</Form.Label>
       <Form.Control 
         type="number" 
         placeholder="Set Product Stocks" 
           value={stocks}
           onChange={e => setStocks(e.target.value)}
         required
       />
       <Form.Text className="text-muted">  </Form.Text>
   </Form.Group> 
   <p></p>

      <Form.Group controlId="Categories" id='form2'>
        <Form.Label>Category</Form.Label>
        <Form.Control
          as="select" 
          value={category}
          onChange={(e) => setCategory(e.target.value)}
          required
        >
          <option value="">Select a category</option> 
          <option value="Bags">Bags</option> 
          <option value="Coffee">Coffee</option>
          <option value="Car_Accessories">Car Accessories</option>
          <option value="Computers">Computers</option>
        </Form.Control>
        <Form.Text className="text-muted">Choose a category for the product</Form.Text>
      </Form.Group>

            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">  	Submit    </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
            	Submit    </Button>
            }
   
   </Form>
   </div>
  );
};



