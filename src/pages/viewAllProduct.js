import React, { useEffect, useState } from "react";
import ProductTable from "./ProductTable";

export default function Products() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        
        fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setProducts(data);
            })
    }, [])

    const handleUpdateProduct = (productId, updatedProduct) => {
        
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedProduct)
        })
            .then(res => res.json())
            .then(updatedData => {
                console.log(`Updated product with ID: ${productId}`, updatedData);
        
                setProducts(products.map(product => product._id === productId ? updatedData : product));
            })
            .catch(error => console.error(`Failed to update product with ID: ${productId}`, error));
    }

    const handleDeleteProduct = (productId) => {
        // Delete the product from the database
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'DELETE',
        })
            .then(() => {
                console.log(`Deleted product with ID: ${productId} from the database`);
    

                setProducts(products.filter(product => product._id !== productId));
    
            })
            .catch(error => console.error(`Failed to delete product with ID: ${productId}`, error));
    }
    

    return (
        <>
            <ProductTable
                products={products}
                onUpdateProduct={handleUpdateProduct}
                onDeleteProduct={handleDeleteProduct}
            />
        </>
    )
}
