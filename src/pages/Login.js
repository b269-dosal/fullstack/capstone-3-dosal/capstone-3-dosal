import Swal from 'sweetalert2';
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import './Login.css';
import backgroundImage from '../login3.jpeg'; 



export default function Login(){
   
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
   
            console.log(data);

 
            if(typeof data.access !== "undefined") {
      
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: 'Welcome back!',
                    html: 'You have successfully logged in.',
                    icon: 'success',
                    confirmButtonText: 'Continue Shopping',
                    showCloseButton: true,
                    customClass: {
                      title: 'swal2-title',
                      htmlContainer: 'swal2-html-container',
                      confirmButton: 'swal2-confirm-button',
                      closeButton: 'swal2-close-button'
                    }
                  }).then(() => {
                    window.location.href = '/';
                  });
               
                
         }  
          else {

         Swal.fire({
            title: "Authentication Failed!",
            text: "Please check your login details and try again",
            icon: "error",
            confirmButtonText: "OK",
            confirmButtonColor: "#FF0000",
            customClass: {
                title: 'my-custom-title-class',
                content: 'my-custom-content-class',
                confirmButton: 'my-custom-confirm-button-class'
            },
            showCloseButton: true,
            closeButtonHtml: '<i class="fas fa-times"></i>',
            footer: '<a href="/forgotPassword">Forgot password?</a>',
            backdrop: `
                rgba(0,0,0,0.8)
                center left
                no-repeat
            `,
            timer: 5000,
            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            cancelButtonColor: "#808080",
            reverseButtons: true,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve();
                    }, 2000);
                });
            }
        }).then((result) => {

            if (result.isConfirmed) {
   
              } else if (result.isDenied) {
                
              }

        }).catch((error) => {

        }); 

    }
    });

        setEmail('')
        setPassword('')
};
    const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        };

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    const navigated = useNavigate();
    
    const handleButtonClick = () => {        
        navigated("/userRegistration");
      };


    return(
        (user.id !== null) ?
        <Navigate to="/products" />
        :

        <div className="login-container" id='BackImage' 
            style={{ 
                borderRadius: '30px',
                backgroundImage: `url(${backgroundImage})`, 
                backgroundSize: 'cover', 
                backgroundPosition: 'center', 
            }}>
    
        <div className="login-container" id='containerBox3' style={{ borderRadius: '30px' }}>
        <h1>LOGIN</h1>
        
                <Form onSubmit={e => authenticate(e)}>

                    <Form.Group controlId="userEmail">
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />          
                    </Form.Group>

                    <Form.Group controlId="password">
            
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>
                <p></p>

                    {   isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }
                    <p></p>

                    <Link to="/forgotPassword">Forgot Password</Link>
                    <p></p>
                    <Button variant="success" onClick={handleButtonClick}>Create New Account</Button>
                </Form>
        </div>
</div>

    )
};


