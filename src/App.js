import {useEffect, useState} from 'react'
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import {Container} from 'react-bootstrap';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './components/Errorpage';
import AddProduct from './pages/addProduct';
import ViewAllProduct from './pages/viewAllProduct';
import UpdateProduct from './components/UpdateProduct';
import CatalogPage from './components/Catalog';
import CartPage from './pages/CartPage';
import RetrieveOrderNon from './pages/RetrieveOrderNon';
import RetrieveOrderAdmin from './pages/RetrieveOrderAdmin';
import AdminUsersAccount from './pages/AdminUsersAccount';
import EditProfile from './components/EditProfile';
import ForgotPasswordForm from './components/ForgotPassword';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';

export default function App() {


  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

      // User is Logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);


  return (
    // <></> fragments - common pattern in React for component to return multiple element
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
        
          <Container>
            
            <Routes>
            <Route path="/update/:productId" element={<UpdateProduct/>}/>
            <Route path="/" element={<Home/>}/>
            <Route path="/userRegistration" element={<Register/>}/> 
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/*" element={<Error/>}/>
            <Route path="/productRegistration" element={<AddProduct/>}/> 
            <Route path="/allProducts" element={<ViewAllProduct/>}/>
            <Route path="/products" element={<CatalogPage/>}/>
            <Route path="/cart" element={<CartPage/>}/>
            <Route path="/orderMade" element={<RetrieveOrderNon/>}/>
            <Route path="/orderAdmin" element={<RetrieveOrderAdmin/>}/>
            <Route path="/nonAdminUsers" element={<AdminUsersAccount/>}/>
            <Route path="/editProfile/:userId" element={<EditProfile/>}/>
            <Route path="/forgotPassword" element={<ForgotPasswordForm/>}/>



            </Routes>  
            
          </Container> 

        </Router>
      </UserProvider>
    </>
  );
}

