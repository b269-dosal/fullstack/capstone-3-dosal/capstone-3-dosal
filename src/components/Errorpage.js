import { Button, Row, Col } from "react-bootstrap";
import {Link} from 'react-router-dom'


export default function Error() {
return (
    <Row id="row1">
    	<Col className="p-5">
            <h1>Oops!</h1>
            <p>The page you are looking for cannot be found</p>
            <Link to="/">
            <Button variant="primary">Back to Home</Button>
            </Link>
        </Col>
    </Row>
	)
} 

