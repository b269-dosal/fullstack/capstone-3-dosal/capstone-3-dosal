import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

const SingleProduct = () => {
  const { productId } = useParams();
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [stocks, setStocks] = useState("");
  const [updatedProductName, setUpdatedProductName] = useState("");
  const [updatedDescription, setUpdatedDescription] = useState("");
  const [updatedPrice, setUpdatedPrice] = useState("");
  const [updatedStocks, setUpdatedStocks] = useState("");
 

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })
      .then(response => response.json())
      .then(data => {

        setProductName(data.prod_name);
        setDescription(data.prod_desc);
        setPrice(data.prod_price);
        setStocks(data.stocks)

        setUpdatedProductName(data.prod_name);
        setUpdatedDescription(data.prod_desc);
        setUpdatedPrice(data.prod_price);
        setUpdatedStocks(data.stocks)
      })
      .catch(error => {
        console.error("Error fetching product data:", error);
      });


  }, [productId]);

  const handleUpdate = () => {
    
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        prod_name: updatedProductName,
        prod_desc: updatedDescription,
        prod_price: updatedPrice,
        stocks: updatedStocks
      })
    })
      .then(response => response.json())
      .then(data => {
        console.log(data)
        setProductName(data.prod_name);
        setDescription(data.prod_desc);
        setPrice(data.prod_price);
        setStocks(data.stocks);

        
        Swal.fire({
          title: 'Update Succesful',
          html: 'You have successfully Updated the Product.',
          icon: 'success',
          confirmButtonText: 'Continue Updating',
          showCloseButton: true,
          customClass: {
            title: 'swal2-title',
            htmlContainer: 'swal2-html-container',
            confirmButton: 'swal2-confirm-button',
            closeButton: 'swal2-close-button'
          }
        }).then(() => {
          window.location.href = '/allProducts';
        });



      })
      .catch(error => {
        console.error("Error updating product data:", error);
      });
  };


  const handleBack = () => {
    window.location.href ='/allProducts'
  }


  return (
    <div id="updateProduct">
    <div className="login-container2">
    <section>
      <h4>{productId}</h4>
      <p>Product Name: {productName}</p>
      <p>Product Description: {description}</p>
      <p>Product Price: {price}</p>
      <p>Product Stocks: {stocks}</p>


      <input
        type="text"
        value={updatedProductName}
        onChange={e => setUpdatedProductName(e.target.value)}
      />
      <input
        type="text"
        value={updatedDescription}
        onChange={e => setUpdatedDescription(e.target.value)}
      />
      <input
        type="number"
        value={updatedPrice}
        onChange={e => setUpdatedPrice(e.target.value)}
      />
      <input
        type="number"
        value={updatedStocks}
        onChange={e => setUpdatedStocks(e.target.value)}
      />
      <p></p>
      <Button 
      onClick={handleUpdate} 
      variant="primary"
      style={{ width: '50%' }}>
      Update</Button>

      <Button
          onClick={handleBack}
          variant="danger"
          style={{ width: '50%' }}>
          BACK
      </Button>

    </section>
    </div>
    </div>
  );

};

export default SingleProduct;
