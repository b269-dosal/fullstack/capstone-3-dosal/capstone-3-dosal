
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const FrontPage = () => {
  return (
    <Container className="mt-3">
      <h1 className="text-center mb-4">Welcome to My eCommerce Store</h1>
      <Row xs={1} sm={2} md={4} lg={4} className="g-4">
        {/* Catalog Image 1 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://images.philips.com/is/image/PhilipsConsumer/271V8_71-IMS-en_PH?$jpglarge$&wid=1250"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 1</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 2 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://ph-live-02.slatic.net/p/976a90d4a2b9dd4c76ac56ac55011feb.jpg"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 2</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 3 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
		  <img
                src="https://m.media-amazon.com/images/I/61ugNy9xqaL._UL1280_.jpg"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
			  </a>
            <div className="catalog-caption">
              <h4>Catalog 3</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 4 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/5b0981ff-45f8-40c3-9372-32430a62aaea/dunk-high-womens-shoes-cF9txG.png"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 4</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 5 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://cdn.thewirecutter.com/wp-content/media/2022/09/totebags-2048px-3945.jpg?auto=webp&quality=75&crop=2:1&width=1024"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 5</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 6 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://cdn.shopify.com/s/files/1/0442/2749/4055/products/pulse-ed-1_1024x1024.jpg?v=1611810601"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 6</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
            </div>
          </div>
        </Col>

        {/* Catalog Image 7 */}
        <Col>
          <div className="catalog-image">
		  <a href="/">
              <img
                src="https://media.wired.com/photos/6250790a01affead6abdee74/1:1/w_1800,h_1800,c_limit/Asus_RT-AX86U-SOURCE-Asus-Gear.jpg"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
            <div className="catalog-caption">
              <h4>Catalog 7</h4>
              <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
           
			  </div>
      </div>
    </Col>

    {/* Catalog Image 8 */}
    <Col>
      <div className="catalog-image">
	  <a href="/">
              <img
                src="https://d1rlzxa98cyc61.cloudfront.net/catalog/product/cache/1801c418208f9607a371e61f8d9184d9/1/7/173733_2020.jpg"
                alt="Catalog 1"
                className="img-fluid"
				style={{ height: "200px", width: "100%" }}
              />
            </a>
        <div className="catalog-caption">
          <h4>Catalog 8</h4>
          <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
        </div>
      </div>
    </Col>
  </Row>
</Container>

);
};

export default FrontPage;