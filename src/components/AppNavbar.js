import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { Nav, Navbar } from 'react-bootstrap';
import './AppNav.css';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="warning
    " expand="lg" id="NavBar">
      <Navbar.Brand as={Link} to="/">
        <strong>SHOP</strong>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link
            as={NavLink}
            to="/"
            className="nav-link"
            activeClassName="active"
          >
            Home
          </Nav.Link>

          {user.id !== null ? (
            <>
              {user.isAdmin === false && (
                <Nav.Link
                  as={NavLink}
                  to="/products"
                  className="nav-link"
                  activeClassName="active"
                >
                  Products
                </Nav.Link>
              )}
              {user.isAdmin && (
                <Nav.Link
                  as={NavLink}
                  to="/productRegistration"
                  className="nav-link"
                  activeClassName="active"
                >
                  Add
                </Nav.Link>
              )}
              {user.isAdmin && (
                <Nav.Link
                  as={NavLink}
                  to="/allProducts"
                  className="nav-link"
                  activeClassName="active"
                >
                  All Product
                </Nav.Link>
              )}
              {user.isAdmin === false && (
                <Nav.Link
                  as={NavLink}
                  to="/cart"
                  className="nav-link"
                  activeClassName="active"
                >
                  Cart
                </Nav.Link>
              )}
              {user.isAdmin === false && (
                <Nav.Link
                  as={NavLink}
                  to="/orderMade"
                  className="nav-link"
                  activeClassName="active"
                >
                  OrderMade
                </Nav.Link>
              )}
              {user.isAdmin === true && (
                <Nav.Link
                  as={NavLink}
                  to="/orderAdmin"
                  className="nav-link"
                  activeClassName="active"
                >
                  OrderMade
                </Nav.Link>
              )}
              {user.isAdmin === true && (
                <Nav.Link
                  as={NavLink}
                  to="/nonAdminUsers"
                  className="nav-link"
                  activeClassName="active"
                >
                  User Profile
                </Nav.Link>
              )}
              <Nav.Link
                as={NavLink}
                to="/logout"
                className="nav-link"
                activeClassName="active"
              >
                Logout
              </Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link
                as={NavLink}
                to="/products"
                className="nav-link"
                activeClassName="active"
              >
                Products
              </Nav.Link>
              <Nav.Link
                as={NavLink}
                to="/login"
                className="nav-link"
                activeClassName="active"
              >
                Login
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}




//////////// Version 2
/* 
import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { Nav, Navbar, NavItem, NavLink as SidebarLink } from 'react-bootstrap';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
    <>
      {user.isAdmin ? (
        <div id="sidebar" style={{ backgroundColor: 'gray', padding: '10px' }}>
          <Nav className="flex-column">
            <NavItem>
              <SidebarLink as={NavLink} to="/" activeClassName="active" exact>Home</SidebarLink>
            </NavItem>
            <NavItem>
              <SidebarLink as={NavLink} to="/productRegistration" activeClassName="active">Add Product</SidebarLink>
            </NavItem>
            <NavItem>
              <SidebarLink as={NavLink} to="/allProducts" activeClassName="active">All Products</SidebarLink>
            </NavItem>
            <NavItem>
              <SidebarLink as={NavLink} to="/orderAdmin" activeClassName="active">Order Made</SidebarLink>
            </NavItem>
            <NavItem>
              <SidebarLink as={NavLink} to="/nonAdminUsers" activeClassName="active">Non-Admin Users</SidebarLink>
            </NavItem>
            <NavItem>
              <SidebarLink as={NavLink} to="/logout" activeClassName="active">Logout</SidebarLink>
            </NavItem>
          </Nav>
        </div>
      ) : (
       <Navbar bg="light" expand="lg" id="NavBar">
          <Navbar.Brand as={Link} to="/"><strong>SHOP</strong></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
              {user.isAdmin === false && user.id === false && <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>}
              {user.isAdmin === false && user.id === false && <Nav.Link as={NavLink} to="/orderMade">Order Made</Nav.Link>}
              {user.isAdmin === true && user.id === true && <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>}
              {user.id === null &&<Nav.Link as={NavLink} to="/login">Login</Nav.Link>}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      )}
    </>
  );
} */

////// Version 3
/* 

import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { Nav, Navbar, NavItem, NavLink as SidebarLink, Button } from 'react-bootstrap';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
    <div style={{ display: 'flex' }}>
      <div id="sidebar" style={{ backgroundColor: 'gray', padding: '10px', width: '250px', position: 'fixed', height: '100vh' }}>

        <Nav className="flex-column">
          <NavItem>
            <Button as={NavLink} to="/" activeClassName="active" exact variant="dark" block>Home</Button>
          </NavItem>
          <NavItem>
            <Button as={NavLink} to="/productRegistration" activeClassName="active" variant="dark" block>Add Product</Button>
          </NavItem>
          <NavItem>
            <Button as={NavLink} to="/allProducts" activeClassName="active" variant="dark" block>All Products</Button>
          </NavItem>
          <NavItem>
            <Button as={NavLink} to="/orderAdmin" activeClassName="active" variant="dark" block>Order Made</Button>
          </NavItem>
          <NavItem>
            <Button as={NavLink} to="/nonAdminUsers" activeClassName="active" variant="dark" block>User Profile</Button>
          </NavItem>
          <NavItem>
            <Button as={NavLink} to="/logout" activeClassName="active" variant="dark" block>Logout</Button>
          </NavItem>
        </Nav>
      </div>
      <div style={{ marginLeft: '250px', padding: '10px', width: '100%' }}>

        {user.isAdmin ? (

          <div>
            <h1>Welcome, Admin!</h1>

          </div>
        ) : (
      
          <div>
            <Navbar bg="light" expand="lg" id="NavBar">
              <Navbar.Brand as={Link} to="/"><strong>SHOP</strong></Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Button as={NavLink} to="/" exact variant="light">Home</Button>

                  <Button as={NavLink} to="/products" variant="light">Products</Button>
                  {user.isAdmin === false && user.id === false && <Button as={NavLink} to="/cart" variant="light">Cart</Button>}

                  {user.isAdmin === false && user.id === false && <Button as={NavLink} to="/orderMade" variant="light">Order Made</Button>}
                  {user.isAdmin === true && user.id === true && <Button as={NavLink} to="/logout" variant="light">Logout</Button>}
                  {user.id === null &&<Button as={NavLink} to="/login" variant="light">Login</Button>}
                </Nav>
              </Navbar.Collapse>
            </Navbar>
    
          </div>
        )}
      </div>
    </div>
  );
}
 */