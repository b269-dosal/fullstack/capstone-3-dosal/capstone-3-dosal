import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

const UserProfile = () => {
  const { userId } = useParams();
  console.log("Requested UserID: ");
  console.log(userId);
  const [user, setUser] = useState(null);
  const [editing, setEditing] = useState(false);
  const [formData, setFormData] = useState({
    id: "",
    firstName: "",
    lastName: "",
    email: "",
    mobileNumber: "",
    password: "*****",
    password2: "*****",
    isAdmin: false
  });

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/users/${userId}/userDetails`,
          {
            method: "POST"
          }
        );
        const data = await response.json();
        setUser(data);
        setFormData({
          id: data._id,
          firstName: data.first_Name,
          lastName: data.last_Name,
          email: data.email,
          mobileNumber: data.mobileNo,
          password: data.password,
          isAdmin: data.isAdmin
        });
        console.log("User Profile Here: ");
        console.log(data);
      } catch (error) {
        console.error("Failed to fetch users:", error);
      }
    };
    fetchUsers();
  }, [userId]);

  const handleEdit = () => {
    setEditing(true);
  };

  const handleSave = () => {
    console.log("This is User Account");
    console.log(formData);

    const {
      id,
      firstName,
      lastName,
      email,
      mobileNumber,
      password,
      isAdmin
    } = formData;

    fetch(`${process.env.REACT_APP_API_URL}/users/updateUser/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        first_Name: firstName,
        last_Name: lastName,
        email: email,
        password: password,
        mobileNo: mobileNumber,
        isAdmin: isAdmin
      })
    })
      .then(response => response.json())
      .then(data => {

        console.log("What happen here:")
        console.log(data)

        if (data) {


          Swal.fire({
            icon:  'success',
            title: `Updated Account: ${email}`,
            text:  `${email} has been updated`,
          });

          setEditing(false);
          setUser(prevUser => ({
            ...prevUser,
            first_Name: firstName,
            last_Name: lastName,
            email: email,
            mobileNo: mobileNumber,
            password: password,
            isAdmin: isAdmin
          }));
        } 
        else {
         console.log("Technical Error")
        }
      })
      
    
  };

  const handleChange = e => {
    const { name, value } = e.target;
    setFormData(prevFormData => ({
      ...prevFormData,
      [name]: value
    }));
  };


  const isSaveButtonDisabled = () => {
    const password1 = formData.password;
    const password2 = formData.password2;
    console.log(password1)
    console.log(password2)
  
     
   return formData.password !== formData.password2 || formData.password === "" || formData.password2 === ""
  
    };
     

  return (
    <div id="editProfile">
    <div>
      {user ? (
        <div>
          {editing ? (
            <>
              <h1>Edit Profile</h1>
              <form>
                <label>ID:</label>

                <input type="text" name="id" value={user._id} disabled />

                <label>First Name:</label>
                <input
                  type="text"
                  id="firstName"
                  name="firstName"
                  value={formData.firstName}
                  onChange={handleChange}
                />

                <label>Last Name:</label>
                <input
                  type="text"
                  id="lastName"
                  name="lastName"
                  value={formData.lastName}
                  onChange={handleChange}
                />

                <label>Email:</label>
                <input
                  type="email"
                  id="email"
                  name="email"
                  value={formData.email}
                  disabled
                />

                <label>Mobile Number:</label>
                <input
                  type="number"
                  id="mobileNumber"
                  name="mobileNumber"
                  value={formData.mobileNumber}
                  onChange={handleChange}
                  maxLength={11}
                
                />


                <label>Password:</label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  value={formData.password}
                  onChange={handleChange}
                />

                
                <label>Confirm Password:</label>
                <input
                  type="password"
                  id="password"
                  name="password2"
                  value={formData.password2}
                  onChange={handleChange}
                />

                <label>Is Admin:</label>
                <input
                  type="checkbox"
                  name="isAdmin"
                  checked={formData.isAdmin}
                  onChange={() =>
                    setFormData((prevUser) => ({ ...prevUser, isAdmin: !prevUser.isAdmin }))
                  }
                />
                <p></p>

                <Button className="bg-success" onClick={handleSave} disabled={isSaveButtonDisabled()}>Submit</Button>
                <p></p>
                <Button className="bg-danger"  style={({
                width: "100%"})} as={Link} to={`/nonAdminUsers`}>Back</Button>
              </form>
              
            </>
          ) : (
            <>
              <h1>User Profile</h1>
              <p></p>
              <p>ID: {user._id}</p>
              <p>First Name: {user.first_Name}</p>
              <p>Last Name: {user.last_Name}</p>
              <p>Email: {user.email}</p>
              <p>Mobile Number: {user.mobileNo}</p>
              <p>Is Admin: {user.isAdmin ? "Yes" : "No"}</p>

            <Button className="bg-primary"  style={({
                width: "100%"})} onClick={handleEdit}>Edit Profile</Button>
<p></p>
            <Button className="bg-danger"  style={({
                width: "100%"})} as={Link} to={`/nonAdminUsers`}>Go Back</Button>

            </>
          )}
        </div>
      ) : (
        <p>Loading user data...</p>
      )}
    </div>
    </div>
  );
};

export default UserProfile;
