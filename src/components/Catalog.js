import React, { useState, useContext, useEffect } from "react";
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from "sweetalert2";


const ActiveProducts = () => {

  const {user} = useContext(UserContext);
  const UserID = user.id
  console.log(`USERID ${UserID}`);
  const isAdmin = user.isAdmin;
  console.log(isAdmin)
  
  const [products, setProducts] = useState([]);
  const [quantity2, setQuantity] = useState([])
  

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(response => response.json())
      .then(data => {
        setProducts(data);

      })
      .catch(error => {
        console.error("Error fetching product data:", error);
      });
  }, []);
  
/// Quantity
  const handleQuantityChange = (event) => {
    const value = event.target.value;
    if (!isNaN(value)) {
      setQuantity(parseInt(value));
    }
  };

  const handleOrder = (product) => {
    setQuantity(0);

    const productID = product._id;

    const productNAME = product.prod_name;
    const productPRICE = product.prod_price;


    if (isAdmin) {
      console.log('Admin user cannot create orders.');
      return;
    }
    
 const totalAMOUNT = quantity2 * productPRICE;
 console.log(`Quantiy here: ${quantity2}`)
  if (quantity2 > product.stocks) {
    Swal.fire({
      icon: 'error',
      title: 'Invalid Quantity',
      text: 'Entered quantity exceeds available stocks',
    });
    
    console.log("Number of Quantity: ")
    console.log(quantity2)
    return
  } 
  else {
    Swal.fire({
      icon: 'success',
      title: 'Add to Cart',
      text: `You have Successfully Ordered ${productNAME}, total amount of ${totalAMOUNT}`,
    }).then(() => {
    
      window.location.href = '/products';
    });
 
  }
    fetch(`${process.env.REACT_APP_API_URL}/order/createOrder/${UserID}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
        body: JSON.stringify({
        userId: UserID,
        productId: productID,
        productName: productNAME,
        productPrice: productPRICE,
        quantity: quantity2,   
                                                 
      })
    })
      .then(response => {
        if (response) {
          console.log("Order placed:", product);
        } else {
          console.error("Error placing order:", response.statusText);
        }
      })
      .catch(error => {
        console.error("Error placing order:", error);
      });
  };
  
  

  return (

    <div id="containerBox" style={{ borderRadius: '30px' }}>
   <div className="text-center" id="header" >
      <h1 id="header_font">PRODUCTS</h1>
    </div>
    <Row>
      {products.map((product, index) => (
        <Col key={product.id} sm={12} md={6} lg={4} className="mb-4">
        <Card className={'bg-warning'}>
            <Card.Img variant="top" src={product.image} />
            <Card.Body>
              <Card.Title>{product.prod_name}</Card.Title>
              <h5>Price: P{product.prod_price}</h5>
              <h6>Stocks: {product.stocks}</h6>
              <h8>Description: {product.prod_desc}</h8>
              <p></p>
              
              <input
                type="number"
                min="1"
                max={product.stocks}
                value={quantity2[product._id]}
                onChange={handleQuantityChange}
                className="form-control mb-3"
                step="1" // Add step attribute to enable increment and decrement arrows
              />

            <p></p>
              {user.id === null ? (
               <Link to="/login">              
                  <Button variant="primary" style={{width: '100%'}}>Login to buy</Button>
                </Link>

              ) : (
                <>
                    {user.id === null ? (
                      <Link to="/login">
                        <Button variant="primary" style={{ width: '50%' }}>Login to buy</Button>
                      </Link>
                    ) : (
                      <Button variant="success" onClick={() => handleOrder(product)} style={{ width: '50%' }}>
                        Add to Cart
                      </Button>
                    )}

                    <Link to="/cart">
                      <Button variant="danger" style={{ width: '50%' }}>
                       Checkout
                      </Button>
                  </Link>

                </>
              )}

            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  </div>

  );

}


export default ActiveProducts;









