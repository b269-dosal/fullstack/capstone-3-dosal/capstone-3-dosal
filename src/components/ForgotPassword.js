import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import '../pages/Login.css';
import Swal from 'sweetalert2';


const ForgotPasswordForm = () => {
  const [email, setEmail] = useState('');
  const [newPassword, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  ///// Function to Check Email if Exist 

  const handleSubmit = (e) => {
    e.preventDefault();
    if (newPassword !== password2) {
      console.log('Password mismatch error');
      return;
    }
    
    // Check if the email exists
    fetch(`${process.env.REACT_APP_API_URL}/users/emailcheck`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email }),
    })
    .then(res => res.json())
    .then(emailExists => {
        console.log("What is this")
        console.log(emailExists)

        const oldPassword = emailExists.password;
        console.log("Old Password: ");
        console.log(oldPassword);
 
      if (emailExists) {
     
        console.log(`Email Exist: `);
        console.log(email);
        console.log(newPassword);

                
        fetch(`${process.env.REACT_APP_API_URL}/users/updatePassword`, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: email,
            password: newPassword,
          }),
        })
        .then(res => res.json())
        .then(updatePasswordResponse => {
          console.log("What is this");
          console.log(updatePasswordResponse);
  
          if (updatePasswordResponse) {
            console.log('Password update successful');
            Swal.fire({
              title: "Password Update Successful",
              icon: "success",
              text: "You have successfully updated your Password"
            }).then(() => {
             
                window.location.href = "/login";
              });
            }
          else {
            console.log('Password update error');
            Swal.fire({
              title: "Password Update Failed",
              icon: "error",
              text: "Password Update Failed"
            });
          }
        })
        .catch(error => {
          console.error('Fetch error:', error);
   
        });
      } 
      
      else {
        Swal.fire({
          title: "Email Doesn't Exist",
          icon: "error",
          text: "Would you like to register?",
          footer: '<a href="/userRegistration">Register?</a>'
        });
      } 

    })

     .catch(error => {
      console.error('Fetch error:', error);
 
    }); 
    
  };
  
  // Password Confirmation Checking
  const isSubmitDisabled = () => {
    if (newPassword === "" || password2 === "" || newPassword !== password2) {
      return true;
    } else {
      return false;
    }
  };
  
  
  const handleReturnClick = () => {        
    window.location.href = `/login`;
    };
 

  return (
    <div id='editProfile'>
    <div className="login-container">
     
        <h2>Forgot Password</h2>
        <p></p>

        <form onSubmit={handleSubmit}>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter Valid Email"
            required
          />

          <label htmlFor="password1">New Password:</label>
          <input
            type="password"
            id="password1"
            name="password1"
            value={newPassword}
            onChange={(e) => setPassword1(e.target.value)}
            placeholder="New Password"
            required
          />

          <label htmlFor="password2">Confirm Password:</label>
          <input
            type="password"
            id="password2"
            name="password2"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
            placeholder="Confirm Password"
            required
          />

          <Button type="submit" variant="success" disabled={isSubmitDisabled()}>
            Submit
          </Button>
         

        </form>
      
        <Button type="submit" variant="danger" onClick={handleReturnClick}  style={{ width: '100%' }}>
          Return
        </Button>
   
    </div>
    </div>
  );
};

export default ForgotPasswordForm;
